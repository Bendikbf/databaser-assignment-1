<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            try {
                $this->db = new PDO('mysql:host=localhost;dbname=databaser assignment 1;charset=utf8', 'root','',
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                // Create PDO connection
            } catch (PDOException $e) {
                $view = new errorView('Connection failed: ' . $e->getMessage());
                $view->create();
            }
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
        try {
            $booklist = array();
            $temp = $this->db->query('SELECT * FROM book ORDER BY id');
            while($row = $temp->fetch(PDO::FETCH_ASSOC)) {
                $bookTemp = new Book($row['title'] , $row['author'] , $row['description'] , $row['id']);
                $booklist[] = $bookTemp;
            }
        } catch (PDOException $e) {
            $view = new errorView('Failed to get book list: ' . $e->getMessage());
            $view->create();
        }
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
        if (is_numeric($id)) {
        try {
            $temp = $this->db->prepare("SELECT * FROM book WHERE id=:id");
            $temp->bindValue(":id", $id);
            $temp->execute();
            $book = $temp->fetch(PDO::FETCH_OBJ);
        } catch (PDOException $e) {
            $view = new errorView('failed to find book: ' . $e->getMessage());
            $view->create();
        }
        return $book;
        } else {
            $view = new errorView('invalid id.');
            $view->create();
        }
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
        if (!empty($book->title) && !empty($book->author))
        try {
            $temp = $this->db->prepare("INSERT INTO Book (title, author, description) VALUES(:title, :author, :description)");
            $temp->bindValue(":title", $book->title);
            $temp->bindValue(":author", $book->author);
            $temp->bindValue(":description", $book->description);
            $book->id = 1;              //For testing.
            $temp->execute();
        } catch (PDOException $e) {
            $view = new errorView('failed to add book: ' . $e->getMessage());
            $view->create();
        }  else {
            $view = new errorView('invalid author or title input.');
            $view->create();
        } 
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        if (is_numeric($book->id) && !empty($book->title) && !empty($book->author)) {
        try {
            $temp = $this->db->prepare("UPDATE Book SET title=:title, author=:author, description=:description WHERE id=$book->id");
            $temp->bindValue(":title", $book->title);
            $temp->bindValue(":author", $book->author);
            $temp->bindValue(":description", $book->description);
            $temp->execute();
        } catch (PDOException $e) {
            $view = new errorView('failed to modify book: ' . $e->getMessage());
            $view->create();
        }
        } else {
            $view = new errorView('Invalid input. ');
            $view->create();
        } 
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        if (is_numeric($id)) {
        try {
            $temp = $this->db->prepare("DELETE FROM Book WHERE id=$id");
            $temp->execute();
        } catch (PDOException $e) {
            $view = new errorView('failed to delete book: ' . $e->getMessage());
            $view->create();
        }
        } else {
            $view = new errorView('id input invalid.');
            $view->create();
        }
    }
}

?>